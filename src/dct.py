from util import seeds
from validate import validate
from sklearn.tree import DecisionTreeClassifier

def Run(train_x, train_y, test_x, prefix='base'):
	seeds()

	model = DecisionTreeClassifier()
	model.fit(train_x, train_y.values.ravel())

	train_pred = model.predict(train_x)
	test_pred = model.predict(test_x)

	validate(f'DecisionTree-{prefix}', train_pred, test_pred)
