#!/usr/bin/env python
from data import *
from util import seeds, savefig
from validate import validate
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import code

seeds()

code.interact(local=locals())
