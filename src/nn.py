#!/usr/bin/env python
from util import seeds, savefig
from data import *
from validate import validate
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Dropout

def Run(Xs, Ys, Ts):
	seeds()

	model = Sequential()

	model.add(Dense(units=1024, input_shape=(Xs.shape[1],), kernel_initializer='uniform', activation='sigmoid'))
	model.add(Dense(units=1024, kernel_initializer='uniform', activation='sigmoid'))
	model.add(Dense(units=1024, kernel_initializer='uniform', activation='sigmoid'))
	model.add(Dropout(.67))
	model.add(Dense(units=Ys.shape[1], activation='sigmoid'))

	model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

	hist = model.fit(Xs, Ys, epochs=100)

	train_pred = model.predict_classes(Xs)
	test_pred = model.predict_classes(Ts)

	return (train_pred, test_pred, hist.history['acc'])

def Disp(train_pred, test_pred, hist, prefix='base'):
	validate(f'NeuralNetwork-{prefix}', train_pred, test_pred)

	plt.plot(hist)
	savefig(f'NeuralNetwork-{prefix}-history.png')

if __name__ == '__main__':
	base = Run(train_x, train_y, test_x)
	scaled = Run(train_scaled, train_y, test_scaled)
	poly = Run(train_poly, train_y, test_poly)
	best = Run(train_best, train_y, test_best)

	Disp(base[0], base[1], base[2])
	Disp(scaled[0], scaled[1], scaled[2], 'scaled')
	Disp(poly[0], poly[1], poly[2], 'poly')
	Disp(best[0], best[1], best[2], 'best')
