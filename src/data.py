from util import data_file, savefig
from config import seed
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, PolynomialFeatures

_le = LabelEncoder()
_pca = PCA(n_components=1)
_poly = PolynomialFeatures(2)
_best = SelectKBest(k=4)

with open(data_file('titanic_passengers.info')) as f:
	info = f.read()

frame = pd.read_csv(data_file('titanic_passengers.csv'))

frame = frame.drop('PassengerId', axis=1)
frame = frame.drop('Name', axis=1)
frame = frame.drop('Ticket', axis=1)
frame = frame.drop('Cabin', axis=1)
frame = frame.dropna()

frame['Sex'] = _le.fit_transform(frame['Sex'])
frame['Embarked'] = _le.fit_transform(frame['Embarked'])

X = frame.drop('Survived', axis=1)
Y = frame['Survived'].to_frame(name='Survived')

scaled_x = _pca.fit_transform(X)
poly_x = _poly.fit_transform(X)
best_x = _best.fit_transform(X, Y.values.ravel())

train_x, test_x = train_test_split(X, random_state=seed)
train_y, test_y = train_test_split(Y, random_state=seed)
train_scaled, test_scaled = train_test_split(scaled_x, random_state=seed)
train_poly, test_poly = train_test_split(poly_x, random_state=seed)
train_best, test_best = train_test_split(best_x, random_state=seed)
