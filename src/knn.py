from util import seeds
from validate import validate
from sklearn.neighbors import KNeighborsClassifier

def Run(train_x, train_y, test_x, prefix='base'):
	seeds()

	model = KNeighborsClassifier()
	model.fit(train_x, train_y.values.ravel())

	train_pred = model.predict(train_x)
	test_pred = model.predict(test_x)

	validate(f'KNeighbors-{prefix}', train_pred, test_pred)
