#!/usr/bin/env python
from util import savefig
from data import frame, info, X, Y
import matplotlib.pyplot as plt
import seaborn as sns

if __name__ == '__main__':
	print(info)
	frame.info()

	sns.heatmap(frame.corr(), center=0, annot=True)
	savefig('res-corr.png')

	sns.pairplot(frame)
	savefig('res-pairs.png')

	fig, ax = plt.subplots()
	sns.distplot(Y, kde=False)
	ax.set(
		xlabel='Survived',
		ylabel='Amount'
	)
	savefig('res-surv.png')

	sns.heatmap(X.corr().abs(), vmin=0.8, vmax=1, annot=True)
	savefig('res-coll.png')
