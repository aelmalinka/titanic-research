from config import seed
from os import path, mkdir
from sys import argv
import matplotlib.pyplot as plt

def data_file(fname, dpath='./data'):
    if len(argv) >= 2:
        dpath = argv[1]

    return path.join(dpath, fname)

def savefig(fname):
	plt.savefig(out_file(fname))
	plt.clf()
	plt.cla()

def out_file(fname, opath='./out'):
	if not path.exists(opath):
		mkdir(opath)

	return path.join(opath, fname)

def seeds(v=seed):
	# 2020-03-23 AMR NOTE: not setting tensorflow to single threaded
	# 2020-03-23 AMR TODO: is this a problem?
	from os import environ
	from random import seed as pyseed
	from numpy.random import seed as npseed
	from tensorflow.random import set_seed

	environ['PYTHONHASHSEED'] = str(v)
	pyseed(v)
	npseed(v)
	set_seed(v)
