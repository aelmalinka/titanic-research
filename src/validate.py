from data import train_y, test_y
from util import savefig
import seaborn as sns
from sklearn.metrics import classification_report, confusion_matrix

def validate(name, train_pred, test_pred):
	print(f'{name} Training')
	print(classification_report(train_y, train_pred))

	sns.heatmap(confusion_matrix(train_y, train_pred), annot=True)
	savefig(f'{name}-train.png')

	print(f'{name} Testing')
	print(classification_report(test_y, test_pred))

	sns.heatmap(confusion_matrix(test_y, test_pred), annot=True)
	savefig(f'{name}-test.png')
