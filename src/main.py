#!/usr/bin/env python
from data import train_y, train_x, test_x, train_scaled, test_scaled, train_poly, test_poly, train_best, test_best
import knn
import gnb
import dct
import svc

if __name__ == '__main__':
	knn.Run(train_x, train_y, test_x)
	gnb.Run(train_x, train_y, test_x)
	dct.Run(train_x, train_y, test_x)
	svc.Run(train_x, train_y, test_x)

	knn.Run(train_scaled, train_y, test_scaled, 'scaled')
	gnb.Run(train_scaled, train_y, test_scaled, 'scaled')
	dct.Run(train_scaled, train_y, test_scaled, 'scaled')
	svc.Run(train_scaled, train_y, test_scaled, 'scaled')

	knn.Run(train_poly, train_y, test_poly, 'poly')
	gnb.Run(train_poly, train_y, test_poly, 'poly')
	dct.Run(train_poly, train_y, test_poly, 'poly')
	svc.Run(train_poly, train_y, test_poly, 'poly')

	knn.Run(train_best, train_y, test_best, 'best')
	gnb.Run(train_best, train_y, test_best, 'best')
	dct.Run(train_best, train_y, test_best, 'best')
	svc.Run(train_best, train_y, test_best, 'best')
